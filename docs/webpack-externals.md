The externals configuration option provides a way of excluding dependencies from the output bundles. Instead, the created bundle relies on that dependency to be present in the consumer's environment. This feature is typically most useful to library developers, however there are a variety of applications for it.

외부 구성 옵션은 출력 번들에서 종속성을 제외시키는 방법을 제공합니다. 대신 생성 된 번들은 소비자 환경에 존재하는 의존성에 의존합니다. 이 기능은 일반적으로 라이브러리 개발자에게 가장 유용하지만 다양한 응용 프로그램이 있습니다.

consumer here is any end user application that includes the library that you have bundled using webpack.
여기에있는 소비자는 webpack을 사용하여 번들로 제공 한 라이브러리가 포함 된 최종 사용자 응용 프로그램입니다.


Prevent bundling of certain imported packages and instead retrieve these external dependencies at runtime.
특정 가져온 패키지를 번들링하지 않고 런타임에 이러한 외부 종속성을 검색합니다.

For example, to include jQuery from a CDN instead of bundling it:
예를 들어 번들로 묶는 대신 CDN에서 jQuery를 포함하려면 다음과 같이하십시오.

<script src="https://code.jquery.com/jquery-3.1.0.js"
  integrity="sha256-slogkvB1K3VOkzAI8QITxV3VzpOnkeNVsKvtkYLMjfk="
  crossorigin="anonymous"></script>


webpack.config.js
  externals: {
    jquery: 'jQuery'
  }

This leaves any dependent modules unchanged, i.e. the code shown below will still work:
이렇게하면 종속 모듈이 변경되지 않고 남아 있습니다. 즉, 아래에 표시된 코드가 계속 작동합니다.

import $ from 'jquery';

$('.my-element').animate(...);


The bundle with external dependencies can be used in various module contexts, such as CommonJS, AMD, global and ES2015 modules. The external library may be available in any of these forms:
외부 종속성이있는 번들은 CommonJS, AMD, 글로벌 및 ES2015 모듈과 같은 다양한 모듈 컨텍스트에서 사용할 수 있습니다. 외부 라이브러리는 다음과 같은 형식으로 사용할 수 있습니다.

root - An external library can be available as a global variable. The consumer can achieve this by including the external library in a script tag. This is the default setting for externals.
root - 외부 라이브러리를 전역 변수로 사용할 수 있습니다. 소비자는 외부 라이브러리를 스크립트 태그에 포함시켜이를 달성 할 수 있습니다. 이는 외부에 대한 기본 설정입니다.

commonjs - The consumer application may be using a CommonJS module system and hence the external library should be available as a CommonJS module.
commonjs - 소비자 응용 프로그램이 CommonJS 모듈 시스템을 사용할 수 있으므로 외부 라이브러리가 CommonJS 모듈로 사용 가능해야합니다.

commonjs2 - Similar to the above line but where the export is module.exports.default.
commonjs2 - 위의 행과 유사하지만 export가 module.exports.default 인 곳.

amd - Similar to the above line but using AMD module system.
amd - 위의 행과 유사하지만 AMD 모듈 시스템을 사용합니다.

externals accepts various syntax and interprets them in different manners.
externals는 다양한 구문을 허용하고 다양한 방식으로 해석합니다.


#string
jQuery in the externals indicates that your bundle will need jQuery variable in the global form.
외부의 jQuery는 번들에 전역 형식의 jQuery 변수가 필요함을 나타냅니다.

#array
externals: {
  subtract: ['./math', 'subtract']
}
subtract: ['./math', 'subtract'] converts to a parent child construct, where ./math is the parent module and your bundle only requires the subset under subtract variable.
./math가 부모 모듈이고 번들에 subtract 변수 아래의 하위 집합 만 있으면됩니다.

#object
externals : {
  react: 'react'
}

// or

externals : {
  lodash : {
    commonjs: "lodash",
    amd: "lodash",
    root: "_"  // indicates global variable
  }
}

// or

externals : {
  subtract : {
    root: ["math", "subtract"]
  }
}

This syntax is used to describe all the possible ways that an external library can be available. lodash here is available as lodash under AMD and CommonJS module systems but available as _ in a global variable form. subtract here is available via the property subtract under the global math object (e.g. window['math']['subtract']).

이 구문은 외부 라이브러리를 사용할 수있는 모든 가능한 방법을 설명하는 데 사용됩니다. lodash는 AMD 및 CommonJS 모듈 시스템에서 lodash로 사용할 수 있지만 전역 변수 형식으로 사용할 수 있습니다. 여기서 빼는 것은 전역 수학 오브젝트 아래의 속성 빼기를 통해 사용할 수 있습니다 (예 : window [ 'math'] [ 'subtract']).


#function
It might be useful to define your own function to control the behavior of what you want to externalize from webpack. webpack-node-externals, for example, excludes all modules from the node_modules and provides some options to, for example, whitelist packages.

webpack에서 외부화하려는 동작을 제어하는 자체 함수를 정의하는 것이 유용 할 수 있습니다. 예를 들어, webpack-node-externals는 node_modules에서 모든 모듈을 제외하고 패키지를 허용 목록과 같은 일부 옵션을 제공합니다.

It basically comes down to this:

externals: [
  function(context, request, callback) {
    if (/^yourregex$/.test(request)){
      return callback(null, 'commonjs ' + request);
    }
    callback();
  }
],
The 'commonjs ' + request defines the type of module that needs to be externalized.
'commonjs'+ request 는 외부화해야하는 모듈 유형을 정의합니다.


#regex
Every dependency that matches the given regular expression will be excluded from the output bundles.
주어진 정규 표현식과 일치하는 모든 종속성은 출력 번들에서 제외됩니다.

externals: /^(jquery|\$)$/i
In this case any dependency named jQuery, capitalized or not, or $ would be externalized.
이 경우 jQuery, capitalized 또는 not라는 이름의 종속성 또는 $가 외부화됩니다.
