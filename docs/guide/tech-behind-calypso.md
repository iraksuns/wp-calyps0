# The Technology Behind Calypso

Understanding the technologies and abstractions on which Calypso is built can make for a much better learning and building experience. Even if you are a student of the “learning by doing” school, we would still encourage you spend some time with the resources in this document.

칼립소가 구축 된 기술과 추상화를 이해하면 학습 및 건축 경험이 훨씬 향상 될 수 있습니다. "배우면서 배우는"학교의 학생이라 할지라도, 우리는이 문서의 자료들과 시간을 보내시기 바랍니다.

## No Big Famous Framework

Calypso is using neither Angular nor Ember; because we are building Calypso for the long haul, updating and improving a home-grown framework is a better long-term approach for us. Currently, we're using React and Flux/Redux; they're great, but knowing we have the control to use better technologies as they come along makes us feel more confident in our future. Calypso isn't a small startup project; we know it will need to scale and our technology will need to scale with it.

칼립소는 Angular 또는 Ember를 사용하지 않습니다. 장기간에 걸쳐 칼립소를 구축하고 있기 때문에 자체 개발 프레임 워크를 업데이트하고 개선하는 것이 장기적으로 더 나은 접근 방법입니다. 현재 우리는 React and Flux / Redux를 사용하고 있습니다. 그들은 훌륭하지만 우리가 앞으로 나아갈 때 더 나은 기술을 사용할 수 있다는 것을 알고 있으면 우리의 미래에 대해 더 자신감을 가질 수 있습니다. 칼립소는 작은 시작 프로젝트가 아닙니다. 우리는 확장이 필요하다는 것을 알고 있고 우리 기술은 그 기술로 확장해야합니다.

## Modern Modular JavaScript

Instead of a named framework, we decided to assemble our own out of small, focused JavaScript modules.

저명한 프레임 워크 대신 작은 자체 JavaScript 모듈을 자체적으로 조합하기로 결정했습니다.

In the recent years, JavaScript went a long way from the language hidden behind jQuery selectors to the engine behind huge single-page applications and fast node.js async server-side daemons. ES6 is a confirmation the language is going in the right direction.

최근 몇 년 동안 JavaScript는 jQuery 선택기 뒤에 숨어있는 언어에서 거대한 단일 페이지 응용 프로그램과 빠른 node.js 비동기 서버 측 데몬에 이르는 엔진에 이르기까지 긴 시간을 보냈습니다. ES6은 언어가 올바른 방향으로 가고 있는지 확인합니다.

Here are few resources to get up to speed with “modern” JavaScript and ES6:

"현대적인"JavaScript 및 ES6을 사용하여 속도를 높이는 데 필요한 몇 가지 리소스가 있습니다.

* [JavaScript Allongé, the "Six" Edition](https://leanpub.com/javascriptallongesix/read)
* [Exploring ES6](http://exploringjs.com/es6/)
* [What the heck is the event loop anyway?](https://www.youtube.com/watch?v=8aGhZQkoFbQ) – short presentation that sheds some light on how asynchronous operations are executed in JavaScript

Key concepts checklist:

* [Module pattern with CommonJS](http://darrenderidder.github.io/talks/ModulePatterns/) and [npm](https://docs.npmjs.com)
* Scope, context, and [function binding](http://dailyjs.com/2012/06/24/this-binding/)
* [Basic prototypes – creating new objects, inheritance](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Inheritance_and_the_prototype_chain)
* Higher-level functions – [`map`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map), [`filter`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter), [`reduce`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce)
* Async primitives – [callbacks](https://docs.nodejitsu.com/articles/getting-started/control-flow/what-are-callbacks), [promises](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Promise)

*Do you know any good videos and presentations on the subject? If yes, please send a pull request to add them here.*

*주제에 대한 좋은 동영상과 프레젠테이션을 알고 있습니까? 예인 경우 여기에 추가하라는 요청을 보내주십시오.*

## React

[React](http://facebook.github.io/react/) is a library by Facebook that implements a virtual DOM. Instead of carefully changing the DOM, we can just re-render whole components when the data changes. React radically simplified our code and allowed for an awesome composition of reusable components.

[React] (http://facebook.github.io/react/)는 Facebook에 의한 가상 DOM을 구현하는 라이브러리입니다. DOM을 신중하게 변경하는 대신 데이터가 변경되면 전체 구성 요소를 다시 렌더링 할 수 있습니다. 코드를 근본적으로 간단하게 처리하고 재사용 가능한 구성 요소를 구성 할 수 있습니다.

Here are some great React resources:

* [Official documentation](http://facebook.github.io/react/docs/getting-started.html)
* [React.js Introduction For People Who Know Just Enough jQuery To Get By](http://reactfordesigners.com/labs/reactjs-introduction-for-people-who-know-just-enough-jquery-to-get-by/)
* [Tutorial at Scotch.io](https://scotch.io/tutorials/learning-react-getting-started-and-concepts)
* [ReactJS For Stupid People](http://blog.andrewray.me/reactjs-for-stupid-people/)
* [Thinking in React](http://facebook.github.io/react/docs/thinking-in-react.html)
* [Official Tutorial](http://facebook.github.io/react/docs/tutorial.html)
* Presentation: [Rethinking Best Practices](https://www.youtube.com/watch?v=x7cQ3mrcKaY)
* Presentation: [Be Predictable, Not Correct](https://www.youtube.com/watch?v=h3KksH8gfcQ)
* Presentation: [Why does React Scale?](https://www.youtube.com/watch?v=D-ioDiacTm8)

Key concepts checklist:

* What is mounting of components?
* When are components rendered?
* What happens on render?
* [Differences between props and state](http://facebook.github.io/react/docs/interactivity-and-dynamic-uis.html)
* [Component lifecycle methods](http://facebook.github.io/react/docs/component-specs.html)
* [Mixins](http://facebook.github.io/react/docs/reusable-components.html#mixins)
* [Why shouldn’t we touch the DOM the old way](http://facebook.github.io/react/docs/working-with-the-browser.html)

## Redux

All new code uses [Redux](http://redux.js.org/) to manage state in Calypso – making sure all components work with and react to the same data, living in a single place, instead of scattered between components’ state. Older code uses various [Flux](https://facebook.github.io/flux/) implementations or other data components.

새로운 모든 코드는 [Redux] (http://redux.js.org/)를 사용하여 Calypso에서 상태를 관리합니다. 모든 구성 요소가 동일한 데이터에 작동하고 반응하며 구성 요소간에 흩어져있는 대신 한 곳에서 살아가는 것을 확인합니다. 상태. 이전 코드는 다양한 [Flux] (https://facebook.github.io/flux/) 구현 또는 기타 데이터 구성 요소를 사용합니다.

Few, but solid Redux resources:

* [The official website](http://redux.js.org)
* Probably the best way to learn Redux is via the Egghead [Get Started with Redux](https://egghead.io/courses/getting-started-with-redux) video course
* When good with the basics, the [Building React Applications with Idiomatic Redux](https://egghead.io/courses/building-react-applications-with-idiomatic-redux) video course is also super useful (though we do few things differently in Calypso, like routing)
* The [Ecosystem](http://redux.js.org/docs/introduction/Ecosystem.html) page on the official site has a lot of links to tutorials and examples
* For more Calypso-specific details, see the [Our Approach to Data](../our-approach-to-data.md) document

## Git

Calypso is developed on Github, and we use Git extensively. Git is extremely powerful, and understanding how it works and controlling it are an important part of our daily workflow.

Essential Git resources:

* The [Pro Git](http://git-scm.com/book/en/v2) book is online and free. It's a great resource, both for beginners and for intermediate users (few dare to call themselves advanced).
* [git ready](http://gitready.com) – byte-sized tips
* Several shorter articles with tips:
	- [A few git tips you didn't know about](http://mislav.uniqpath.com/2010/07/git-tips/)
	- [25 Tips for Intermediate Git Users](https://www.andyjeffries.co.uk/25-tips-for-intermediate-git-users/)
	- [Stupid Git Tricks](http://webchick.net/stupid-git-tricks)
	- [9 Awesome Git Tricks](http://www.tychoish.com/posts/9-awesome-git-tricks/)
* Some operations are easier using a GUI. [GitX](http://rowanj.github.io/gitx/) is a simple one for OS X. [Fugitive](https://github.com/tpope/vim-fugitive) is a must for `vim`. The GitHub app doesn’t entirely fit our workflow, but you can use it for pulling and committing. One caveat is that you will have to do all rebasing manually.

Key concepts checklist:

* How is Git different from Subversion?
* How does branching work? Why are branches cheap?
* What is rebasing? How is it different from merging?
* What happens when we run `git pull`?
* What’s a remote? What happens when we push to it?
* Which parts of the repository are kept locally and which remotely?
* What’s the staging area? Why is this extra step useful?
* What is squashing? How can we edit and reorder commits before pushing/merging?

The way we use Git with Calypso is described in the [Git Workflow document](../git-workflow.md).

## Other technologies used in Calypso, worth checking out:

* [page.js](http://visionmedia.github.io/page.js/) – router
* [express.js](http://expressjs.com) – light server-side framework we use to serve the initial page
* [lodash](https://lodash.com) – general purpose utility library; includes a ton of useful functions for dealing with arrays, objects, and collections
* [webpack](http://webpack.github.io) – building a JavaScript bundle of all of our modules and making sure loading them works just fine
* [Babel](https://babeljs.io) – for transpiling ES2015+ and JSX

Previous: [Hello, World!](hello-world.md) Next: [Glossary of Terms](glossary.md)
