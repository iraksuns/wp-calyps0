Isomorphic Routing Middleware Adapters
======================================

## index.js

This module provides functions to adapt `page.js`-style middleware (with
`( context, next )` signature) to work with `express.js` (`( req, res, next )`),
so `client` middleware satisfying a couple of constraints can be re-used on the
server side.

이 모듈은 express.js ((req, res, next))와 작동하도록 page.js 스타일 미들웨어 ((컨텍스트, 다음) 시그니처와 함께)를 조정하는 기능을 제공하므로 몇 가지 제약 조건을 만족하는 클라이언트 미들웨어를 재사용 할 수 있습니다. 서버 쪽.

## loader.js

The Webpack loader is required in order for the dynamic module `require()`
(code splitting!) in index.js to work.

index.js의 동적 모듈 require () (코드 분할!)가 작동하려면 Webpack 로더가 필요합니다.

For more information, see [Isomorphic Routing docs](docs/isomorphic-routing.md).
