function getSectionsModule( sections ) {
	console.log( '>>>>> server/isomorphic-routing >>>>> loader.js >>>>> getSectionsModule' );
	var caseSections = '';
	sections.forEach( function( section ) {
		if ( section.isomorphic ) {
			caseSections += 'case ' + JSON.stringify( section.module ) + ': return require( ' + JSON.stringify( section.module ) + ' );\n';
		}
	} );

	const result = [
		'module.exports = {',
		'	get: function() {',
		'		return ' + JSON.stringify( sections ) + ';',
		'	},',
		' require: function( module ) {',
		'		switch ( module ) {',
		caseSections,
		'			default:',
		'				return null;',
		'   }',
		' }',
		'};'
	].join( '\n' );

	return result;
}

module.exports = function( content ) {
	console.log( '>>>>> server/isomorphic-routing >>>>> loader.js' );
	var sections;

	this.cacheable && this.cacheable();

	sections = require( this.resourcePath );

	if ( ! Array.isArray( sections ) ) {
		this.emitError( 'Chunks module is not an array' );
		return content;
	}

	return getSectionsModule( sections );
};
