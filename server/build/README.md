build
=====

Tiny Express.js middleware that spawns `npm run build-css`
from the application root directory for every request to regenerate the CSS.
See [the bundler](../bundler/README.md) for in-depth information on JavaScript bundle regeneration.

npm을 생성하는 작은 Express.js 미들웨어는 CSS 재생성 요청마다 응용 프로그램 루트 디렉토리에서 build-css를 실행합니다. JavaScript 번들 재생성에 대한 자세한 정보는 번들러를 참조하십시오.
