/**
 * External dependencies
 */
var express = require( 'express' );

/**
 * Internal dependencies
 */
var version = require( '../../package.json' ).version,
	config = require( 'config' ),
	oauth = require( './oauth' );

module.exports = function() {
	console.log( '>>>>> server/api/index.js' );
	var app = express();

	app.get( '/version', function( request, response ) {
		console.log( '>>>>> server/api/index.js >>>>> get version' );
		response.json( {
			version: version
		} );
	} );

	if ( config.isEnabled( 'oauth' ) ) {
		oauth( app );
	}

	return app;
};
