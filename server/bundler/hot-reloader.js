/***** WARNING: ES5 code only here. Not transpiled! *****/

/**
 * External dependencies
 */
var socketio = require( 'socket.io' );
var debug = require( 'debug' )( 'calypso:bundler:hot-reloader' );
var cssHotReloader = require( './css-hot-reload' );

var io = null,
	_stats = null,
	hotReloader;

function invalidPlugin() {
	console.log( '>>>>> server/bundeler/hot-reloader.js >>>>> invalidPlugin' );
	if ( io ) {
		io.emit( 'invalid' );
	}
}

function sendStats( socket, stats, force ) {
	console.log( '>>>>> server/bundeler/hot-reloader.js >>>>> sendStats' );
	// stats는 코드 전체 오브젝트
	function emitted( asset ) {
		return ! asset.emitted;
	}

	if ( ! force && stats && stats.assets && stats.assets.every( emitted ) ) {
		return socket.emit( 'still-ok' );
	}

	socket.emit( 'hash', stats.hash );

	if ( stats.errors.length > 0 ) {
		socket.emit( 'errors', stats.errors );
	} else if ( stats.warnings.length > 0 ) {
		socket.emit( 'warnings', stats.warnings );
	} else {
		socket.emit( 'ok' );
	}
}

hotReloader = {

	listen: function( server, webpackCompiler ) {
		console.log( '>>>>> server/bundeler/hot-reloader.js >>>>> listen' );
		io = socketio.listen( server, { 'log level': 1 } );
		io.sockets.on( 'connection', function( socket ) {
			console.log( '>>>>> server/bundeler/hot-reloader.js >>>>> connection' );
			socket.emit( 'hot' );
			if ( ! _stats ) {
				return;
			}
			sendStats( socket, _stats.toJson(), true );
		} );

		webpackCompiler.plugin( 'compile', invalidPlugin );
		webpackCompiler.plugin( 'invalid', invalidPlugin );
		webpackCompiler.plugin( 'done', function( stats ) {
			if ( ! io ) {
				return;
			}
			sendStats( io.sockets, stats.toJson() );
			_stats = stats;
		} );

		// CSS hot reloading logic

		io.of( '/css-hot-reload' ).on( 'connection', function() {
			console.log( '>>>>> server/bundeler/hot-reloader.js >>>>> css-hot-reload connection' );
			debug( '/css-hot-reload new connection' );
		} );

		cssHotReloader( io );
	},

	close: function() {
		console.log( '>>>>> server/bundeler/hot-reloader.js >>>>> close' );
		if ( io ) {
			io.close();
			io = null;
		}
	},

};
console.log( '>>>>> server/bundeler/hot-reloader.js' );
module.exports = hotReloader;
