/* eslint-disable no-trailing-spaces */
/**
 * External dependencies
 */
const fs = require( 'fs' );
const path = require( 'path' );

console.log( '\n>>>>> server/bundler/babel/babel-loader-cache-identifire/index.js' );
/**
 * Given a module name, returns the package version
 *
 * @param  {String} id Module name
 * @return {String}    Module version
 */
function getModuleVersion( id ) {
	return require( path.dirname( require.resolve( id ) ).replace( /[\/\\]lib/, '' ) + '/package' ).version;
}

/**
 * Cache identifier string for use with babel-loader. This is an extension of
 * the default cacheIdentifier, including package version from our custom Babel
 * transform plugin to ensure proper cachebusting.
 *
 * babel-loader와 함께 사용할 캐시 식별자 문자열입니다.
 * 이것은 적절한 cachebusting을 보장하기 위해 맞춤형 Babel transform plugin 의 패키지 버전을 포함하여 기본 cacheIdentifier의 확장입니다.
 * @see https://github.com/babel/babel-loader/blob/501d60d/src/index.js#L85-L92
 * @type {String}
 */
module.exports = JSON.stringify( {
	'babel-loader': getModuleVersion( 'babel-loader' ),
	'babel-core': getModuleVersion( 'babel-core' ),
	'babel-plugin-transform-wpcalypso-async': getModuleVersion( '../babel-plugin-transform-wpcalypso-async' ), // TODO: 언제 어떻게 사용되는지 알아볼것 .
	babelrc: fs.readFileSync( path.resolve( __dirname, '../../../../.babelrc' ), 'utf8' ),
	env: process.env.BABEL_ENV || process.env.NODE_ENV
} );
