## happypack
> HappyPack makes webpack builds faster by allowing you to transform multiple files in parallel.

> HappyPack은 여러 파일을 동시에 변환 할 수있게함으로써 webpack 빌드를보다 빠르게 만듭니다.

## hard-source-webpack-plugin
> HardSourceWebpackPlugin is a plugin for webpack to provide an intermediate caching step for modules.
 In order to see results, you'll need to run webpack twice with this plugin: the first build will take the normal amount of time. The second build will be signficantly faster.

> HardSourceWebpackPlugin은 모듈을위한 중간 캐싱 단계를 제공하는 webpack 용 플러그인입니다. 결과를 보려면이 플러그인으로 webpack을 두 번 실행해야합니다. 첫 번째 빌드에는 정상적인 시간이 걸립니다. 두 번째 빌드는 훨씬 빠릅니다.