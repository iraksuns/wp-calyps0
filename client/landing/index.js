/**
 * External dependencies
 *
 * @format
 */

import page from 'page';
import config from 'config';

/**
 * Internal dependencies
 */
import controller from './controller';

export default function() {
	page( '/welcome', controller.welcome );
}
