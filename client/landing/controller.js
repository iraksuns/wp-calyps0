/**
 * External dependencies
 *
 * @format
 */

import ReactDom from 'react-dom';
import React from 'react';
import qs from 'qs';
import { debounce } from 'lodash';
import page from 'page';
import url from 'url';

/**
 * Internal dependencies
 */
import config from 'config';
import DevWelcome from './welcome';

const landings = {
	// Welcome screen
	welcome: function() {
		ReactDom.render( React.createElement( DevWelcome, {} ), document.getElementById( 'primary' ) );
	},
};

export default landings;
