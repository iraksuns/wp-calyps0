# Webpack의 두가지 핵심 철학
* 모든것은 module이다. 
* JS 파일들은 모듈(modules)이 될 수 있다. 또한 다른 모든 것(CSS, Images, HTMLS..)들도 모듈(modules)이 될 수 있다. 즉 require(“myJSfile.js”) 또는 require(“myCSS.css”) 를 할 수 있다. 이는 다루기 쉬운 작은 조각으로 나누거나, 재사용할 수 있다는걸 의미 합니다.

- 당신이 필요한것 그리고 필요한 때에 불러진다(Load).

전형적인 module bundlers는 모든 모듈을 가져 와서, 큰 bundler.js 파일 하나를 생성한다.
그러나 실제 웹에서 bundle.js는 10~15MB이고 영원히 로드 되고 있을수 있다.
그래서 Webpack은 코드 분할 과 여러 “bundle” 파일을 생성하고 비동기적으로 앱의 일부를 로드 하는 다양한 기능이 있으며, 또한 당신이 필요한 것, 필요한 때에 로드 하게 해준다.

# Development(개발모드) VS Production(배포모드)

첫번째 알아야할 점은 Webpack에는 많은 기능을 가지고 있다는 것이다.
일부는 “개발용”, 다른 일부는 “배포용” 그외 일부는 “배포-개발용”에 대한 것이다.

![image](./my-images/1-WCAdMi04IFEWdngK8bkFcw.png "Optional title")

일반적으로 대부분의 프로젝트는 2가지의 Webpack 설정 파일을 가지고 사용한다.

그리고 bundels 파일을 만들기 위하여 package.json 안에 스크립트를 다음과 같이 작성한다.

```
 “scripts”: {
  //npm run build to build production bundles
  “build”: “webpack --config webpack.config.prod.js”,
  //npm run dev to generate development bundles and run dev. server
  “dev”: “webpack-dev-server”
 }
```

# webpack CLI(Command Line Interface) VS webpack-dev-server
module bundler인 Webpack은 2가지 인터페이스를 제공하는 점에 유의해야 한다.

1. Webpack CLI tool - 기본 인터페이스(Webpack이 알아서 설치한다.)
2. webpack-dev-server - Node.js server 사용(nodeJS 설치요망)

### Webpack CLI ( 배포 빌드엔 굿! )

이 도구는 CLI 및 설정 파일(default : webpack.config.js)을 통해 옵션을 가져와서 bundle로 제공하기 위해 Webpack에 제공한다.

> CLI를 사용하여 Webpack을 배우기 시작할 수도 있지만, 대부분 CLI는 배포 빌드할때나 사용 할 것이다.

Usage:
OPTION1 :
```
//Install it globally
$ npm install webpack —g

//Use it at the terminal 
$ webpack //<--Generates bundle using webpack.config.js
```


Option2 :
```
//Install it locally & add it to package.json
$ npm install webpack —save

//Add it to package.json's script 
“scripts”: {
 “build”: “webpack --config webpack.config.prod.js -p”,
 ...
 }

//Use it by running the following:
"npm run build"
```

### webpack-dev-server ( 개발모드로 굿 )
이것은 8080 포트로 실행되는 Express(node.js)서버입니다. 
서버는 내부적으로 Webpack을 호출 합니다. 
그러면 브라우저 리로딩(실시간 로딩), “Hot Module Replacement” 와 같은 추가적인 기능을 사용할 수 있는 이점이 있습니다.

Usage:

OPTION 1:
```
//Install it globally
npm install webpack-dev-server --save

//Use it at the terminal
$ webpack-dev-server --inline --hot
```

OPTION 2:
```
// Add it to package.json's script 
“scripts”: {
 “start”: “webpack-dev-server --inline --hot”,
 ...
 }

// Use it by running 
$ npm start

Open browser at:
http://localhost:8080
```

# Webpack Vs webpack-dev-server options
inline, hot 과 같은 옵션은 오로지 webpack-dev-server에만 있다는 것에 주목할 필요가 있다. 
그리고 hide-modules은 CLI에만 있는 옵션이다.

## webpack-dev-server CLI options Vs config options



# “entry” — String Vs Array Vs Object
“entry”는 root 모듈 또는 시작 지점이 무엇인지 Webpack에게 알려준다. “entry”는 String, Array, Object 형태로 될 수 있다. 이것이 바로 혼란스러운 점인데 다른 타입은 결국 다른 목적에 사용된다고 생각하면 된다.

## entry - Array
만약 당신이 서로간 의존성이 없는 여러개의 파일을 사용하고 싶다면 Array 형식으로 사용하면 된다. 예를 들어 HTML에 “googleAnalystic.js” 가 필요 할수 있다. 그러면 “bundle.js” 끝에 다음과 같이 추가하도록 지정하면 원하는 output이 된다.

```
{
	entry: ['./public/src/index.js', './public/src/googleAnalytics.js'],
	output" {
		path: '/dist',
		filename: 'bundle.js'
	}
}
```

## entry-object
만약 당신이 SPA(Single Page App)가 아니라 다중 페이지 어플리케이션을 개발한다면, entry-object를 사용하여 한번에 다중 bundle을 만들어서 사용 할 수 있다.
아래 설정 파일은 2개의 bundle JS파일을 생성한다.(indexEntry.js , profileEntry.js 가 나오며 index.html, profile.html에 각각에 사용할 수 있다.)

```
{
	entry: {
		"indexEntry" : "./public/src/index.js",
		"profileEntry": "./public/src/profile.js"
	},
	output" {
		path: '/dist',
		filename: '[name].js' // indexEntry.js & profileEntry.js
	}
}
```

### Usage :
```
//profile.html
<script src=”dist/profileEntry.js”></script>

//index.html
<script src=”dist/indexEntry.js”></script>
```

## entry-combination
당신은 entry Object안에 Array 타입을 사용 할 수 있다. 
예를 들어 아래 설정을 보면 3가지 파일이 생성된다.(3개의 파일이 있는 vendor.js 와 index.js , profile.js가 생성된다.)

```
{
	entry: {
		"vender": ['jquery', 'analytics.js', 'optimizely.js'],
		"index": ['./public/src/index.js'],
		"profile": ['./public/src/profile.js']
	},
	output: {
		path: '/dist',
		filename: '[name].js' // vender.js & index.js & profile.js
	}
}
```